/**
 * @file slab.c
 * @author mengdemao (mengdemao19951021@163.com)
 * @version 1.0
 * @date 2021-04-24
 *
 * @brief slab
 *
 * @copyright Copyright (c) 2021  mengdemao
 *
 */
#define pr_fmt(fmt) "slab: " fmt
#include <linux/init.h>
#include <linux/module.h>
#include <kversion.h>

static int __init slab_init(void)
{
	return 0;
}
module_init(slab_init);

static void __exit slab_exit(void)
{
}
module_exit(slab_exit);

MODULE_AUTHOR(KMODULE_AUTHOR);
MODULE_LICENSE(KMODULE_LICENSE);
MODULE_VERSION(KMODULE_VERSION);
